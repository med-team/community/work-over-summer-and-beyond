# Week-1 till Phase-1

NB: This month, I finally became an [official Debian Maintainer](https://nm.debian.org/person/nilesh/) \o/ 

The following work has been accomplished in the month of June:

## Debian-Med team work done:

###		Uploads to NEW:
* [readucks](https://salsa.debian.org/med-team/readucks/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [python-streamz](https://salsa.debian.org/med-team/python-streamz/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [Shovill](https://salsa.debian.org/med-team/Shovill/-/commits/master?author=Nilesh%20Patra) (Accepted)
* [yanosim](https://salsa.debian.org/med-team/yanosim/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [ngmlr](https://salsa.debian.org/med-team/ngmlr/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [yanagiba](https://salsa.debian.org/med-team/yanagiba/-/commits/master?author=Nilesh%20Patra) (Accepted)
* [paryfor](https://salsa.debian.org/med-team/paryfor/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [pyrle](https://salsa.debian.org/med-team/pyrle/-/commits/master?author=Nilesh%20Patra) (Accepted)
* [sorted-nearest](https://salsa.debian.org/med-team/sorted-nearest/-/commits/master?author=Nilesh%20Patra) (in NEW)
* [pyranges](https://salsa.debian.org/med-team/pyranges/-/commits/master?author=Nilesh%20Patra) (in NEW)


### 	Autopkgtests added/fixed:
* [biomaj3-core](https://salsa.debian.org/med-team/biomaj3-core/-/commits/master?author=Nilesh%20Patra) (Closed: #961902) (Severity: RC bug)
* [libgff](https://salsa.debian.org/med-team/libgff/-/commits/master?author=Nilesh%20Patra)
* [poretools](https://salsa.debian.org/med-team/poretools/-/commits/master?author=Nilesh%20Patra) - also fixed non-functional binaries
* [norsp](https://salsa.debian.org/med-team/norsp/-/commits/master?author=Nilesh%20Patra)
* [solvate](https://salsa.debian.org/med-team/solvate/-/commits/master?author=Nilesh%20Patra)
* [mencal](https://salsa.debian.org/med-team/mencal/-/commits/master?author=Nilesh%20Patra)
* [seqtools](https://salsa.debian.org/med-team/seqtools/-/commits/master?author=Nilesh%20Patra)
* [vsearch](https://salsa.debian.org/med-team/vsearch/-/commits/master?author=Nilesh%20Patra)
* [seq-gen](https://salsa.debian.org/med-team/seq-gen/-/commits/master?author=Nilesh%20Patra) - will be uploaded after a free software license is approved
* [beads](https://salsa.debian.org/med-team/beads/-/commits/master?author=Nilesh%20Patra)
* [sickle](https://salsa.debian.org/med-team/sickle/-/commits/master?author=Nilesh%20Patra)
* [tiddit](https://salsa.debian.org/med-team/tiddit/-/commits/master?author=Nilesh%20Patra) - also fixed this, as it was completely broken with a missing binary
* [elph](https://salsa.debian.org/med-team/elph/-/commits/master?author=Nilesh%20Patra)
		
### 	Bugs Fixed:
* [circos](https://salsa.debian.org/med-team/circos/-/commits/master?author=Nilesh%20Patra) (Closed: #962247) (Severity: normal)
* [gatb-core](https://salsa.debian.org/med-team/gatb-core/-/commits/master?author=Nilesh%20Patra) (Closed: #960414) (Severity: RC)
* [spdlog](https://salsa.debian.org/med-team/spdlog/-/commits/master?author=Nilesh%20Patra) (Closed: #956694) (Severity: RC)

###		Other Work:
* [nanosv](https://salsa.debian.org/med-team/nanosv/-/commits/master?author=Nilesh%20Patra) - enabled build-time tests (Quality Assurance)
* [pychopper](https://salsa.debian.org/med-team/pychopper/-/commits/master?author=Nilesh%20Patra) - enabled build-time tests (Quality Assurance)
* [seqkit](https://salsa.debian.org/med-team/seqkit/-/commits/master?author=Nilesh%20Patra) - remove dependence on forked go-logging

## Work done with other Debian teams:


## Debian JS Team:
	* Updated the Following:
* [node-compression-webpack-plugin](https://salsa.debian.org/med-team/node-compression-webpack-plugin/-/commits/master?author=Nilesh%20Patra)

## Debian Go Team:
	* Fixed the following RC bugs:
* [golang-pault-go-ykpiv](https://salsa.debian.org/go-team/packages/golang-pault-go-ykpiv/-/commits/debian/sid?author=Nilesh%20Patra) (Closed: #890929)
* [golang-github-karlseguin-ccache](https://salsa.debian.org/go-team/packages/golang-github-karlseguin-ccache/-/commits/master?author=Nilesh%20Patra) (Closed: #952187)


## Debian Python Modules Team:
	* Fixed the following RC bugs:
* [djangorestframework-gis](https://salsa.debian.org/python-team/modules/djangorestframework-gis/-/commits/debian/master?author=Nilesh%20Patra) (Closed: #961901)

## Debian Science Team:
	* Fixed the following RC bugs:
* [python-pweave](https://salsa.debian.org/science-team/python-pweave/-/commits/master?author=Nilesh%20Patra)  (Closed: #962104)

## Debian Matrix Team:
* Fixed [nheko](https://salsa.debian.org/matrix-team/nheko/-/commits/buster-backports?author=Nilesh%20Patra) for backports