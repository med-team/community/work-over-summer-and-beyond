# Community bonding and Week - 1

The following work has been accomplished during community bonding and week - 1:

## Debian-Med team work done:

### 	Autopkgtests added/fixed:
* [freecontact](https://salsa.debian.org/med-team/freecontact/-/commits/master?author=Nilesh%20Patra)
* [codonw](https://salsa.debian.org/med-team/codonw/-/commits/master?author=Nilesh%20Patra)
* [seaview](https://salsa.debian.org/med-team/seaview/-/commits/master?author=Nilesh%20Patra)
* [umap-learn](https://salsa.debian.org/med-team/umap-learn/-/commits/master?author=Nilesh%20Patra) - also fixed build-time tests
* [abacas](https://salsa.debian.org/med-team/abacas/-/commits/master?author=Nilesh%20Patra)
* [kissplice](https://salsa.debian.org/med-team/kissplice/-/commits/master?author=Nilesh%20Patra)
		
### 	Bugs Fixed:
* [openslide-python](https://salsa.debian.org/med-team/openslide-python/-/commits/master?author=Nilesh%20Patra) (Closed: #959564)
* [opensurgsim](https://salsa.debian.org/med-team/opensurgsim/-/commits/master?author=Nilesh%20Patra) (Closed: #960496)


## Work done with other Debian teams:


## Debian JS Team:
	* Helped in Babel7 transition, also initiated and helped complete babel-loader transition
	Fixed the following packages:
* [node-escope](https://salsa.debian.org/js-team/node-escope/-/commits/master?author=Nilesh%20Patra)
* [node-debug](https://salsa.debian.org/js-team/node-debug/-/commits/master?author=Nilesh%20Patra)
* [node-es6-error](https://salsa.debian.org/js-team/node-es6-error/-/commits/master?author=Nilesh%20Patra)
* [node-react-audio-player](https://salsa.debian.org/js-team/node-react-audio-player/-/commits/master?author=Nilesh%20Patra)

## Debian Go Team:
	* Fixed the following RC bugs:
* [prometheus-mongodb-exporter](https://salsa.debian.org/go-team/packages/prometheus-mongodb-exporter/-/commits/debian/sid?author=Nilesh%20Patra) (Closed: #952285)
* [golang-github-optiopay-kafka](https://salsa.debian.org/go-team/packages/golang-github-optiopay-kafka/-/commits/master?author=Nilesh%20Patra) (Closed: #867775)


## Debian R package Team:
	* Fixed Following RC bugs:
* [r-cran-sjmisc](https://salsa.debian.org/r-pkg-team/r-cran-sjmisc/-/commits/master?author=Nilesh%20Patra) (Closed: #961499)
* [r-cran-sjplot](https://salsa.debian.org/r-pkg-team/r-cran-sjplot/-/commits/master?author=Nilesh%20Patra) (Closed: #961498)
* [r-bioc-graph](https://salsa.debian.org/r-pkg-team/r-bioc-graph/-/commits/master?author=Nilesh%20Patra) (Closed: #961591)
