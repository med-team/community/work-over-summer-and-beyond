# Phase-1 till Phase-2

The following work has been accomplished in the month of July

## Debian-Med team work done:

###		Uploads to NEW:
* [vcfanno](https://salsa.debian.org/med-team/vcfanno/-/commits/master?author=Nilesh%20Patra)

### 	Autopkgtests added/fixed:
* [pal2nal](https://salsa.debian.org/med-team/pal2nal/-/commits/master?author=Nilesh%20Patra)
* [edflib](https://salsa.debian.org/med-team/edflib/-/commits/master?author=Nilesh%20Patra) (Fixed tests)
* [mptp](https://salsa.debian.org/med-team/mptp/-/commits/master?author=Nilesh%20Patra)
* [python-py2bit](https://salsa.debian.org/med-team/python-py2bit/-/commits/master?author=Nilesh%20Patra)
* [python-scitrack](https://salsa.debian.org/med-team/python-scitrack/-/commits/master?author=Nilesh%20Patra)
* [dwgsim](https://salsa.debian.org/med-team/dwgsim/-/commits/master?author=Nilesh%20Patra)
* [qrish2](https://salsa.debian.org/med-team/qrish2/-/commits/master?author=Nilesh%20Patra)
* [libpll](https://salsa.debian.org/med-team/libpll/-/commits/master?author=Nilesh%20Patra)
* [trf](https://salsa.debian.org/med-team/trf/-/commits/master?author=Nilesh%20Patra)
* [python-biom-format](https://salsa.debian.org/med-team/python-biom-format/-/commits/master?author=Nilesh%20Patra) (Fixed tests)
* [augur](https://salsa.debian.org/med-team/augur/-/commits/master?author=Nilesh%20Patra)
* [python-geneimpacts](https://salsa.debian.org/med-team/python-geneimpacts/-/commits/master?author=Nilesh%20Patra)
* [python-deeptoolsintervals](https://salsa.debian.org/med-team/python-deeptoolsintervals/-/commits/master?author=Nilesh%20Patra)
* [python-xopen](https://salsa.debian.org/med-team/python-xopen/-/commits/master?author=Nilesh%20Patra)


###     RC Bugs Fixed:
* [cyvcf2](https://salsa.debian.org/med-team/cyvcf2/-/commits/master) (Closed: #964677)
* [patsy](https://salsa.debian.org/med-team/patsy/-/commits/master) (Closed: #964640)
* [mlv-smile](https://salsa.debian.org/med-team/mlv-smile/-/commits/master) (Closed: #957551) (Also added autopkgtests)
* [prime-phylo](https://salsa.debian.org/med-team/prime-phylo/-/commits/master) (Closed: #957708)
* [roguenarok](https://salsa.debian.org/med-team/roguenarok/-/commits/master) (Closed: #957768)
* [nibabel](https://salsa.debian.org/med-team/nibabel/-/commits/master) (Closed: #964610)
* [seaview](https://salsa.debian.org/med-team/seaview/-/commits/master) (Closed: #957783)
* [saint](https://salsa.debian.org/med-team/saint/-/commits/master) (Closed: #957772)
* [minc-tools](https://salsa.debian.org/med-team/minc-tools/-/commits/master) (Closed: #957542)


## Debian-Go team work done:

###		Uploads to NEW: (All are Debian-Med team package pre-dependencies) (All accepted)
* [golang-github-brentp-irelate](https://salsa.debian.org/go-team/packages/golang-github-brentp-irelate/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-brentp-bix](https://salsa.debian.org/go-team/packages/golang-github-brentp-bix/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-brentp-vcfgo](https://salsa.debian.org/go-team/packages/golang-github-brentp-vcfgo/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-yuin-gluare](https://salsa.debian.org/go-team/packages/golang-github-yuin-gluare/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-brentp-goluaez](https://salsa.debian.org/go-team/packages/golang-github-brentp-goluaez/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-tatsushid-go-prettytable](https://salsa.debian.org/go-team/packages/golang-github-tatsushid-go-prettytable/-/commits/debian/sid?author=Nilesh%20Patra)
* [golang-github-jung-kurt-gofpdf](https://salsa.debian.org/go-team/packages/golang-github-jung-kurt-gofpdf/-/commits/master?author=Nilesh%20Patra)

### Quality Assurance:
* [golang-github-fogleman-gg](https://salsa.debian.org/go-team/packages/golang-github-fogleman-gg/-/commits/master?author=Nilesh%20Patra) - Fix test failure, not yet uploaded. will do once pre-dependencies get accepted

## Debian Python Modules Team:
	* Fixed the following RC bugs:
* [python-transitions](https://salsa.debian.org/python-team/modules/python-transitions/-/commits/debian/master?author=Nilesh%20Patra) (Closed: #963459)
* [python-urwidtrees](https://salsa.debian.org/python-team/modules/python-urwidtrees/-/commits/master?author=Nilesh%20Patra) (Closed: #963317)
* [pytest-instafail](https://salsa.debian.org/python-team/modules/pytest-instafail/-/commits/master?author=Nilesh%20Patra) (Closed: #894705)
* [python-babel](https://salsa.debian.org/med-team/python-babel/-/commits/master) (Closed: #963402)
* [willow](https://salsa.debian.org/med-team/willow/-/commits/master) (Closed: #951990)

## Debian Science Team:
	* Fixed the following RC bugs:
* [sleef](https://salsa.debian.org/science-team/sleef/-/commits/master?author=Nilesh%20Patra) (Closed: #957809)
