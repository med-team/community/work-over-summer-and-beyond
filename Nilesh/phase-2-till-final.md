# Phase-2 till Final Phase

The following work has been accomplished in the month of August

## Debian-Med team work done:

###		Uploads to NEW:
* [seqkit](https://salsa.debian.org/med-team/seqkit/-/commits/master?author=Nilesh%20Patra)

### 	Autopkgtests added/fixed:
* [python-ncls](https://salsa.debian.org/med-team/python-ncls/-/commits/master?author=Nilesh%20Patra)
* [fast5-research](https://salsa.debian.org/med-team/fast5-research/-/commits/master?author=Nilesh%20Patra)
* [quicktree](https://salsa.debian.org/med-team/quicktree/-/commits/master?author=Nilesh%20Patra)
* [psychopy](https://salsa.debian.org/med-team/psychopy/-/commits/master?author=Nilesh%20Patra)
* [wtdbg2](https://salsa.debian.org/med-team/wtdbg2/-/commits/master?author=Nilesh%20Patra)
* [phipack](https://salsa.debian.org/med-team/phipack/-/commits/master?author=Nilesh%20Patra)
* [repeatmasker-recon](https://salsa.debian.org/med-team/repeatmasker-recon/-/commits/master?author=Nilesh%20Patra)
* [pscan-tfbs](https://salsa.debian.org/med-team/pscan-tfbs/-/commits/master?author=Nilesh%20Patra)
* [volpack](https://salsa.debian.org/med-team/volpack/-/commits/master?author=Nilesh%20Patra)
* [scythe](https://salsa.debian.org/med-team/scythe/-/commits/master?author=Nilesh%20Patra)
* [squizz](https://salsa.debian.org/med-team/squizz/-/commits/master?author=Nilesh%20Patra)
* [baitfisher](https://salsa.debian.org/med-team/baitfisher/-/commits/master?author=Nilesh%20Patra)
* [murasaki](https://salsa.debian.org/med-team/murasaki/-/commits/master?author=Nilesh%20Patra)
* [pycoqc](https://salsa.debian.org/med-team/pycoqc/-/commits/master?author=Nilesh%20Patra)
* [transtermhp](https://salsa.debian.org/med-team/transtermhp/-/commits/master?author=Nilesh%20Patra)
* [radiant](https://salsa.debian.org/med-team/radiant/-/commits/master?author=Nilesh%20Patra)
* [stringtie](https://salsa.debian.org/med-team/stringtie/-/commits/master?author=Nilesh%20Patra)
* [centrifuge](https://salsa.debian.org/med-team/centrifuge/-/commits/master?author=Nilesh%20Patra)
* [tnseq-transit](https://salsa.debian.org/med-team/tnseq-transit/-/commits/master?author=Nilesh%20Patra)
* [harvest-tools](https://salsa.debian.org/med-team/harvest-tools/-/commits/master?author=Nilesh%20Patra)
* [sigma-align](https://salsa.debian.org/med-team/sigma-align/-/commits/master?author=Nilesh%20Patra)
* [sweed](https://salsa.debian.org/med-team/sweed/-/commits/master?author=Nilesh%20Patra)
* [python-pymummer](https://salsa.debian.org/med-team/python-pymummer/-/commits/master?author=Nilesh%20Patra)
* [pychopper](https://salsa.debian.org/med-team/pychopper/-/commits/master?author=Nilesh%20Patra)

###     RC Bugs Fixed:
* [kalign](https://salsa.debian.org/med-team/kalign/-/commits/master) (Closed: #966865)
* [seqmagick](https://salsa.debian.org/med-team/seqmagick/-/commits/master) (Closed: #963326)
* [nitime](https://salsa.debian.org/med-team/nitime/-/commits/master) (Closed: #963663)
* [gmap](https://salsa.debian.org/med-team/gmap/-/commits/master?author=Nilesh%20Patra) (Closed: #966863)


## Debian-Go team work done:

###		Uploads to NEW: (All are Debian-Med team package pre-dependencies) (All accepted)
* [golang-rsc-pdf ](https://salsa.debian.org/go-team/packages/golang-rsc-pdf/-/commits/master?author=Nilesh%20Patra)
* [golang-gonum-v1-plot](https://salsa.debian.org/go-team/packages/golang-gonum-v1-plot/-/commits/master?author=Nilesh%20Patra)
* [golang-github-bsipos-thist](https://salsa.debian.org/go-team/packages/golang-github-bsipos-thist/-/commits/master?author=Nilesh%20Patra)

## Debian Python Modules Team:
### Fixed the following RC bugs:
* [flask-testing](https://salsa.debian.org/python-team/modules/flask-testing/-/commits/master?author=Nilesh%20Patra) (Closed: #966960)
* [flask-restful](https://salsa.debian.org/python-team/modules/flask-restful/-/commits/master?author=Nilesh%20Patra) (Closed: #966962)
* [python-cytoolz](https://salsa.debian.org/python-team/modules/python-cytoolz/-/commits/master?author=Nilesh%20Patra) (Closed: #966996)

### Fixed the following test failures:
* [python-tktreectrl](https://salsa.debian.org/python-team/modules/python-tktreectrl/-/commits/master?author=Nilesh%20Patra)

## Debian Science Team:
	* Fixed the following RC bugs:
* [seaborn](https://salsa.debian.org/science-team/seaborn/-/commits/master?author=Nilesh%20Patra) (Closed: #966991)
* [artha](https://salsa.debian.org/science-team/artha/-/commits/master?author=Nilesh%20Patra) (Closed: #957010)