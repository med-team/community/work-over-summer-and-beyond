# Community bonding and Week - 1

The following work has been accomplished during community bonding and week 1:

### 	Added/fixed examples, docs and autopkgtests:
1.  [hunspell-en-med](https://salsa.debian.org/med-team/hunspell-en-med/-/commits/master?author=Pranav%20Ballaney)
2.  [ncbi-entrez-direct](https://salsa.debian.org/med-team/ncbi-entrez-direct/-/commits/master?author=Pranav%20Ballaney)
3.  [parsnp](https://salsa.debian.org/med-team/parsnp/-/commits/master?author=Pranav%20Ballaney)
4.  [fitgcp](https://salsa.debian.org/med-team/fitgcp/-/commits/master?author=Pranav%20Ballaney)
5.  [tree-puzzle](https://salsa.debian.org/med-team/tree-puzzle/-/commits/master?author=Pranav%20Ballaney)
6.  [filtlong](https://salsa.debian.org/med-team/filtlong/-/commits/master?author=Pranav%20Ballaney)
7.  [cgview](https://salsa.debian.org/med-team/cgview/-/commits/master?author=Pranav%20Ballaney)
8.  [libsmithwaterman](https://salsa.debian.org/med-team/libsmithwaterman/-/commits/master?author=Pranav%20Ballaney)
9.  [transrate-tools](https://salsa.debian.org/med-team/transrate-tools/-/commits/master?author=Pranav%20Ballaney)
10. [biobambam2](https://salsa.debian.org/med-team/biobambam2/-/commits/master?author=Pranav%20Ballaney)
11. [paml](https://salsa.debian.org/med-team/paml/-/commits/master?author=Pranav%20Ballaney)
12. [bandage](https://salsa.debian.org/med-team/bandage/-/commits/master?author=Pranav%20Ballaney)
13. [freebayes](https://salsa.debian.org/med-team/freebayes/-/commits/master?author=Pranav%20Ballaney)
14. [seer](https://salsa.debian.org/med-team/seer/-/commits/master?author=Pranav%20Ballaney)
15. [norsp](https://salsa.debian.org/med-team/norsp/-/commits/master?author=Pranav%20Ballaney)
16. [gasic](https://salsa.debian.org/med-team/gasic/-/commits/master?author=Pranav%20Ballaney)
		
### 	Bugs Fixed:
1. [#954511](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=954511)
   * Build time tests were fixed in bioperl-run
2. [#960368](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=960368)
   * Kallisto now shows and error instead of a segmentation fault.
3. [#806214](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=806214) and [#890790](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=890790)
   * Build time tests had been failing for a long time. Upstream was contacted and it was confirmed that
     failure was only because the output files had not been updated for the last few versions. A patch was written to 
	 fix this issue.
4. [#961013](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=961013) was also investigated, with limited success.
   * It was found that the error was not specific to Debian packaging. 
   The issue was narrowed down and a minimal dataset was created to help the upstream authords debug it. 
   Findings were reported at the [upstream issue tracker](https://github.com/bbuchfink/diamond/issues/351).
